package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"runtime"

	log "bitbucket.org/dream_yun/joinc/logging"
	redis "gopkg.in/redis.v2"
)

type Message struct {
	id   string
	data string
}

type Handler struct {
	logAddr string
	appAddr string
	perfLog log.Log
	appConn *redis.Client
}

func (h Handler) Initalize() error {
	logConn := redis.NewTCPClient(&redis.Options{Addr: h.logAddr})
	h.perfLog = log.Log{App: "apiserver", Conn: logConn}
	err := h.perfLog.Start()
	if err != nil {
		return err
	}

	h.appConn = redis.NewTCPClient(&redis.Options{Addr: h.appAddr})
	_, err = h.appConn.Ping().Result()
	if err != nil {
		return err
	}

	return nil
}

func (h Handler) Run(w http.ResponseWriter, r *http.Request) {
	body, _ := ioutil.ReadAll(r.Body)
	var m Message
	json.Unmarshal(body, &m)
	h.perfLog.Push(m.id)
}

func Check(err error) {
	if err != nil {
		fmt.Print(err.Error())
		os.Exit(1)
	}
}

func main() {
	runtime.GOMAXPROCS(4)
	logAddr := flag.String("log", "localhost:6379", "Loggin REDIS Server")
	appAddr := flag.String("app", "localhost:6379", "Application REDIS Server")

	flag.Parse()

	fmt.Println("Log REDIS Server : ", *logAddr)
	fmt.Println("App REDIS Server : ", *appAddr)
	ApiServer := Handler{logAddr: *logAddr, appAddr: *appAddr}
	err := ApiServer.Initalize()
	Check(err)

	http.HandleFunc("/", ApiServer.Run)
	http.ListenAndServe(":8080", nil)
}
