package logging

import (
	redis "gopkg.in/redis.v2"
	"testing"
)

func TestLogging(t *testing.T) {
	mylog := Log{App: "myapp",
		Conn: redis.NewTCPClient(&redis.Options{Addr: "localhost:6379"})}
	if mylog.Start() != nil {
		t.Error("Redis server connection error!")
	}
	err := mylog.Push("1")
	if err != nil {
		t.Error("Logging error")
	}
}
