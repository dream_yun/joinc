package logging

import (
	//"encoding/json"
	"fmt"
	redis "gopkg.in/redis.v2"
)

type LogFormat struct {
	Id   string
	data string
}

type Log struct {
	App  string
	Conn *redis.Client
}

func (l Log) Start() error {
	_, err := l.Conn.Ping().Result()
	return err
}

func (l Log) Push(msgId string) error {
	tvalue := l.Conn.Time()
	logdata := fmt.Sprintf("{\"id\":\"%s\", \"time\":\"%s.%s\"}", msgId, tvalue.Val()[0], tvalue.Val()[1])
	result := l.Conn.LPush(l.App, logdata)
	return result.Err()
}
