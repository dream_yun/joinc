#!/bin/bash
# Docker Network interface create script 
MACADDR=$(echo $RANDOM|md5sum|sed 's/^\(..\)\(..\)\(..\)\(..\)\(..\).*$/02:\1:\2:\3:\4:\5/')
DOCKER_ID=0
NETNS_DIR="/var/run/netns"
BRIDGE="br0"

IP="0.0.0.0"

USAGE="Usage: $0 
   -d DOCKER_ID     Docker container id
   -i ip            Docker container ip
   -b BRIDGE        Bridge network interface name
"
while [ $# -ge 1 ]; do
	case "$1" in
		-d|-docker-id)
			DOCKER_ID="$2"
			shift 2 ;;
		-i|-ip)
			IP="$2"
			shift 2 ;;
		-b|-bridge)
			BRIDGE="$2"
			shift 2 ;;
	esac
done

if [ $DOCKER_ID == "0" ]
then
	echo "$USAGE"
	exit 1
fi

PID=`docker inspect -f '{{.State.Pid}}' $DOCKER_ID 2> /dev/null`
if [ $? -ne 0 ]
then
	echo "Docket container id error : $DOCKER_ID"
	exit 1
fi

if [ ! -d $NETNS_DIR ]
then
	mkdir -p $NETNS_DIR 
fi
ln -s /proc/$PID/ns/net  /var/run/netns/$PID

peerA="veth$PID-a"
peerB="veth$PID-b"

create_container_network() {
	ip link add $peerA type veth peer name $peerB
	ip link set $peerA up
	ip link set $peerB netns $PID

	ip netns exec $PID ip link set dev $peerB name eth0
	ip netns exec $PID ip link set eth0 address $MACADDR 
	ip netns exec $PID ip link set eth0 up 
	ip netns exec $PID ip addr add $IP/24 dev eth0
	ip netns exec $PID ip route add default via 172.17.42.1
	ovs-vsctl add-port $BRIDGE $peerA
}

#### MAIN ####
create_container_network $PID

cat << EOF 
VETH pair : $peerA & $peerB
Bridge : $BRIDGE 
EOF
